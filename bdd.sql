-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.10-MariaDB-1:10.3.10+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Thé noir'),(2,'Thé vert'),(3,'Infusions');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180907135740'),('20180907141935');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (2,3,'INFUSION GOURMANDE','Une délicieuse infusion BIO de bouleau, argousier, cranberry & myrtille.',15,'feeb3b3869ec9f15731f7d36c0bd6458.jpeg'),(3,2,'THÉ VERT À LA MENTHE','La menthe nanah est celle utilisée pour les thés du Moyen-Orient et de l’Afrique du Nord. Son goût rafraîchissant fait du thé vert à la menthe nanah une boisson très appréciée en fin de repas.',13,'0deb9f9e79e3584167ef53a94972fc1f.jpeg'),(4,2,'THÉ VERT AROMATISÉ GINGEMBRE ET CITRON','Le thé vert gingembre-citron glacé est idéal pour profiter d’un instant de fraîcheur à tout moment de la journée. Sa note de gingembre confère à ce mélange une saveur délicatement épicée qui s’allie à merveille avec le caractère acidulé du citron.',10,'4e462aeadf3c15f29f00a3ff00aaaf9b.jpeg'),(5,2,'THÉ VERT PARFUMÉ AU JASMIN ET BERGAMOTE','Le thé vert de Chine parfumé au jasmin est un grand classique des thés verts aromatisés dont on ne se lasse pas.',17,'c5ebff1f6fdb41e8e8b89d266104c40a.jpeg'),(6,2,'THÉ VERT AROMATISÉ AGRUMES ET FLEUR','Vert Bouquet est une déclinaison de Bouquet de Fleurs N°108, le plus ancien mélange exclusif créé en 1880 par le fondateur de la marque en l’honneur de la naissance de sa première fille Elisabeth. Cette version à base de thé vert aromatisé bergamote, agrumes et fleurs en fait un thé doux et délicat.',17,'7fa5df0ab3d770fe07836cf8a4db83d4.jpeg'),(7,1,'THÉ NOIR AROMATISÉ AGRUMES','Créé en 1888 en hommage à Wladimir le Grand, fondateur de la Sainte Russie, le thé Prince Wladimir est un mélange d\'Earl Grey, d\'agrumes, de vanille et d\'épices. Il est un des incontournables de la maison.',15,'47a4276129b685c7944d8871b29d0966.jpeg'),(8,1,'THÉ NOIR AROMATISÉ, ÉPICES','Figurant parmi les plus anciens mélanges de la maison, son origine remonte probablement au temps des caravanes de thé qui traversaient toute l’Asie jusqu’en Russie par voie terrestre. Kashmir Tchaï est un thé traditionnellement bu en Inde et au Népal avec du lait et du sucre. Ses épices en font l\'accompagnement idéal pour un déjeuner sous le signe de la légèreté.',17,'c407d5a78548882ffaa68edeb5998eed.jpeg'),(9,1,'THÉ NOIR, POIVRE ROSE, GUARANA','Véritable invitation à l’éveil des sens, ce thé offre douceur et sensualité avec son mélange d\'épices, de guarana, de réglisse et de poivre rose au goût naturellement sucré. Son parfum envoûtant ne laissera personne lui résister…',17,'5b322da8bc615b93d9c008d6156cfd62.jpeg'),(10,1,'THÉ NOIR À LA BERGAMOTE','Sans doute le thé noir aromatisé le plus bu au monde, l\'Earl Grey doit son nom au Comte (Earl en anglais) Grey, Premier ministre britannique au début du XIXième siècle. Le parfum relevé et frais de la bergamote (petit citron de Sicile) se marie agréablement bien avec le thé noir.',13,'3359beae3a10e062a7bd9e6b80d276b0.jpeg'),(11,3,'SUREAU, CASSIS, ORAN','Un mélange de plantes et de fruits aux vertus relaxantes, digestives et drainantes.',18,'69ab46383986e2c90c6a8df4389ea912.jpeg'),(12,3,'JARDIN TROPICAL','Mélange solaire et gourmand de mangue, d\'ananas, de papaye et de pêche',9,'5db5705092a621dbe877b3730493e798.jpeg'),(13,3,'JARDIN ROMANTIQUE','Douce infusion de pêche, d’ananas et de myrte citronnée',10,'59a1ae4fa3cf3b772c1f1087eceb896f.jpeg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_line`
--

DROP TABLE IF EXISTS `product_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopping_cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5CFC965745F80CD` (`shopping_cart_id`),
  KEY `IDX_5CFC96574584665A` (`product_id`),
  CONSTRAINT `FK_5CFC96574584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_5CFC965745F80CD` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_cart` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_line`
--

LOCK TABLES `product_line` WRITE;
/*!40000 ALTER TABLE `product_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_72AAD4F6A76ED395` (`user_id`),
  CONSTRAINT `FK_72AAD4F6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'alexia','Joachim','alexia-joachim@hotmail.fr','1996-02-17','$2y$12$1a4OtMrCu1yrS0/aSWsQxO1Lm652W/cHB8mqK20ZGQ4DE1XlQzD5G',1),(2,'Florian','Ligo','pro.alexia.joachim@gmail.com','1989-07-02','$2y$12$stfFg8K4ZwVczCLZ5VtLp.vD9nXm8nbdMcmdM8oJHU5AVQsyP4cKe',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 17:17:06
