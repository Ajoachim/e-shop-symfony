<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
use App\Form\CategoryType;

class CategoryController extends Controller
{
    /**
     * @Route("/admin/add/category", name="add_category")
     */
    public function add(Request $request){

        $category = new Category();
   
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $category = $form->getData();
          
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($category);

            $em->flush();


        } 
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'form' => $form->createView()
        ]);
    }  

    /**
     * @Route("/update/category/{id}", name="update_category")
     */

    public function update(Category $category, int $id, Request $request)
    {
        $form = $this->createForm( CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('home', ["id" => $category->getId()]);
        }

        return $this->render('category/update-category.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('home', ["id" => $category->getId()]);
    }

    /**
    *  @Route("/admin/category/remove/{id}", name="remove_category")
    */
    public function remove(Category $category) {

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute("home", []);
    }

}