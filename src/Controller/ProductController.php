<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Service\FileUploader;
use App\Repository\ProductRepository;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class ProductController extends AbstractController
{
    /**
     * @Route("/add/product", name="add_product")
     */
    public function index(Request $request, FileUploader $fileUploader)
    {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $product = $form->getData();
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);

            $em = $this->getDoctrine()->getManager();

            $em->persist($product);

            $em->flush();

            return $this->redirectToRoute('product', ["id" => $product->getId()]);
        }
        return $this->render('product/index.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/product/{id}", name="product")
     */
    public function show(int $id, ProductRepository $repo, Request $request)
    {

        $product = $repo->find($id);


        return $this->render('product/show.html.twig', [

            "product" => $product

        ]);
    }
    
    /**
     * @Route("/admin/product/remove/{id}", name="remove_product")
     */
    public function remove(Product $product)
    {
     
        $em = $this->getDoctrine()->getManager();

        $em->remove($product);

        $em->flush();

        return $this->redirectToRoute("home", []);
    }


    /**
     * @Route("/admin/product/update/{id}", name="update_product")
     */

    public function update(Product $product, int $id, Request $request, FileUploader $fileUploader)
    {
        $product->setImage(new File("uploads/images/".$product->getImage()));
        dump($product);
        $form = $this->createForm( ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('product', ["id" => $product->getId()]);
        }

        return $this->render('product/update.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('product', ["id" => $product->getId()]);
    }
}
