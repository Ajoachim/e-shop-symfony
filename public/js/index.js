$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})

const sr = ScrollReveal();

sr.reveal('#product-show', {
    origin: 'left',
    duration: 1000,
    delay: 50,
    interval: 150
});


sr.reveal('hr', {
    origin: 'left',
    duration: 1500,
    reset: true,
    delay: 100,
    interval: 150
});







